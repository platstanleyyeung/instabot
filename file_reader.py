# Python program to read
# json file


import json


def read_data_json():
    # Opening JSON file
    f = open('data.json', )

    # returns JSON object as
    # a dictionary
    data = json.load(f)

    # Closing file
    f.close()

    return data
