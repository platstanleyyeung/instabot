# Entry Point for follow

import file_reader
import os
from os.path import join, dirname
from dotenv import load_dotenv
import argparse
from instabot import Bot  # noqa: E402

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

LOGIN_USERNAME = os.environ.get("LOGIN_USERNAME")
LOGIN_PASSWORD = os.environ.get("LOGIN_PASSWORD")

parser = argparse.ArgumentParser(add_help=True)
parser.add_argument("-proxy", type=str, help="proxy")
parser.add_argument("users", type=str, nargs="+", help="users")
args = parser.parse_args()

bot = Bot(
    filter_users=True,
    filter_private_users=False,
    filter_previously_followed=True,
    filter_business_accounts=True,
    filter_verified_accounts=True,
    max_followers_to_follow=20
)
bot.login(
    username=LOGIN_USERNAME,
    password=LOGIN_PASSWORD,
    proxy=args.proxy
)

for username in args.users:
    bot.follow_followers(username)

bot.unfollow_non_followers()